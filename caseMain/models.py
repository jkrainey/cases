from django.db import models

# Create your models here.

class Subject(models.Model):
    lastName = models.CharField(max_length=25)
    firstName= models.CharField(max_length=25)
    middleName= models.CharField(max_length=25)
    emailAddress = models.EmailField()

    def __str__(self):
        return self.lastName + ", " + self.firstName + " " + self.middleName




class Case(models.Model):
    caseNumber = models.CharField(max_length=60)
    description =models.TextField(null=True)
    createdBy = models.ForeignKey('auth.User',on_delete=models.CASCADE,)
    def __str__(self):
     return self.caseNumber + " | " + self.description[:50]

