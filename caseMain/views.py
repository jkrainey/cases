from django.shortcuts import render
from .models import Case
from django.views.generic import ListView
# Create your views here.

def caseMain(req):
    return render(req,"CaseMain.html")



class CaseView(ListView):
    model=Case
    template_name = "CaseMain.html"