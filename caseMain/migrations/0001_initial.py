# Generated by Django 3.1 on 2020-08-08 15:18

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Case',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('caseNumber', models.CharField(max_length=60)),
            ],
        ),
        migrations.CreateModel(
            name='Subject',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lastName', models.CharField(max_length=25)),
                ('firstName', models.CharField(max_length=25)),
                ('middleName', models.CharField(max_length=25)),
                ('emailAddress', models.EmailField(max_length=254)),
            ],
        ),
    ]
